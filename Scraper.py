import requests
import os
import re
from re import search
import time as t
import datetime
import asyncio
from datetime import datetime, date, time, timedelta
from bs4 import BeautifulSoup
import random
import lxml

s = requests.Session()  # requests little things

loop = True  # used to hold program in a forever while loop
scanSite = False  # if in a specific time window chnage to true to start scanning the site
dailyScan = False  # used to scan the main site once a day to get other links
atEveryone = True  # the @ at the end

now = datetime.now()  # computer time tings
currentTime = now.strftime("%H:%M:%S")

proxies = []

# spoof browser to avoid detection
headers = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36'


# all uk tracks
tracks = ["aintree",
          "ascot",
          "ayr",
          "ballinrobe",
          "bangor-on-dee",
          "bath",
          "bellewstown",
          "beverley",
          "brighton",
          "carlisle",
          "cartmel",
          "catterick",
          "chelmsford-city",
          "cheltenham",
          "chepstow",
          "chester",
          "clonmel",
          "cork",
          "curragh",
          "doncaster",
          "down-royal",
          "downpatrick",
          "dundalk",
          "epsom",
          "exeter",
          "fairyhouse",
          "fakenham",
          "ffos-las",
          "fontwell",
          "galway",
          "goodwood",
          "gowran-park",
          "hamilton",
          "haydock",
          "hereford",
          "hexham",
          "huntingdon",
          "kelso",
          "kempton",
          "kilbeggan",
          "killarney",
          "laytown",
          "leicester",
          "leopardstown",
          "limerick",
          "lingfield",
          "listowel",
          "ludlow",
          "market-rasen",
          "musselburgh",
          "naas",
          "navan",
          "newbury",
          "newcastle",
          "newmarket",
          "newton-abbot",
          "nottingham",
          "perth",
          "plumpton",
          "pontefract",
          "punchestown",
          "redcar",
          "ripon",
          "roscommon",
          "salisbury",
          "sandown",
          "sedgefield",
          "sligo",
          "southwell",
          "stratford",
          "taunton",
          "thirsk",
          "thurles",
          "tipperary",
          "towcester",
          "tramore",
          "uttoxeter",
          "warwick",
          "wetherby",
          "wexford",
          "wincanton",
          "windsor",
          "wolverhampton",
          "worcester",
          "yarmouth",
          "york"]


allTrackURLs = []  # urls for every track on the site
allTracks = []  # all tracks on the site with url tagging removed
# raceLinks are the URL's second half of all races e.g "'/horse-racing/2020-09-21-hamilton/14:25/winner'" but list contains unwanted varaible string such as javascript:void(0);
raceLinksWithTrash = []
racesHolder = []  # racesHolder is used as a temporary placement before today's races are removed leaving today's races
races = []  # all today's races
# individualRaceLinks contains all the corret urls to each individual race and time
individualRaceLinks = []
SP_Bet365 = []  # holding all odds for bet365
SP_Boyle = []  # holding all odds for boyle
messageListBet365 = []
messageListBoyle = []
removeFoundLinksList = []


def GetTime():
    global dailyScan
    global scanSite
    global currentTime
    # get current computer time and return string as eg. "1732" for 5:32pm
    now = datetime.now()
    currentTime = now.strftime("%H:%M:%S")
    currentTime = re.sub(':', '', currentTime)
    currentTime = currentTime[:-2]
    # check to see if time is between 12pm and 7pm
    if (int(currentTime) >= 1159 and int(currentTime) <= 2200):
        scanSite = True
    else:
        scanSite = False

    if (int(currentTime) >= 1159 and int(currentTime) <= 1200):
        dailyScan = True
    else:
        dailyScan = False


def OddsLandingPage(allTrackURLs, headers, allTracks, raceLinksWithTrash, racesHolder, races, individualRaceLinks, SP_Bet365, SP_boyle, messageListBet365, messageListBoyle):

    while True:
        try:
            LandingPage = s.get(
                'https://www.oddschecker.com/horse-racing', timeout=3, headers={'User-Agent': headers})
            break
        except requests.exceptions.Timeout:
            print("Timeout occurred")
            LandingPage = s.get(
                'https://www.oddschecker.com/horse-racing', timeout=5, headers={'User-Agent': headers})
            pass

    soup = BeautifulSoup(LandingPage.content, 'html.parser')

    # grab all links
    for links in soup.find_all('a', class_="venue beta-headline"):
        allTrackURLs.append(links['href'])

    allTracks.clear()  # all tracks on the site with url tagging removed
    # raceLinks are the URL's second half of all races e.g "'/horse-racing/2020-09-21-hamilton/14:25/winner'" but list contains unwanted varaible string such as javascript:void(0);
    raceLinksWithTrash.clear()
    # racesHolder is used as a temporary placement before today's races are removed leaving today's races
    racesHolder.clear()
    races.clear()  # all today's races
    # individualRaceLinks contains all the corret urls to each individual race and time
    individualRaceLinks.clear()
    SP_Bet365.clear()  # holding all odds for bet365
    SP_Boyle.clear()  # holding all odds for boyle
    messageListBoyle.clear()
    messageListBet365.clear()


def RemoveURLTagging(allTrackURLs):
    global allTracks
    # remove URL coding from races for discord message
    for _ in allTrackURLs:
        allTracks = [_.replace('/horse-racing/', '')
                     for _ in allTrackURLs]


def GrabAllRaceURLs(headers, raceLinksWithTrash, individualRaceLinks, SP_Bet365):
    for i in races:
        while True:
            try:
                racePages = s.get(
                    'https://www.oddschecker.com/horse-racing' + "/" + i, timeout=3, headers={'User-Agent': headers})
                break
            except requests.exceptions.Timeout:
                print("Timeout occurred")
                racePages = s.get(
                    'https://www.oddschecker.com/horse-racing' + "/" + i, timeout=5, headers={'User-Agent': headers})
                pass

        soup = BeautifulSoup(racePages.content, 'html.parser')

        # grab each individual race form site and remove any random shit we've picked up
        for j in soup.find_all('a', "beta-h3", href=True):
            raceLinksWithTrash.append(j.get('href'))

        # then remove the remaining junk strings in the list
        raceLinksWithTrash.remove('javascript:void(0);')
        raceLinksWithTrash.remove('/tips')

        # if the item for raceLinksWithTrash is not yet in individualRaceLinks input it
        for i in raceLinksWithTrash:
            if i not in individualRaceLinks:
                individualRaceLinks.append(i)

        # make the int array length the same as the amount of links we have
    for x in range(len(individualRaceLinks)):
        SP_Bet365.append("")
        SP_Boyle.append("")

    # set all values string values to nothing
    # for _ in SP_365:
    #   SP_365[_] = ""


# check the site for each race to scrape odds
def CheckOdds(individualRaceLinks, SP_Bet365, messageListBet365, messageListBoyle, removeFoundLinksList):
    positionBet365 = 0
    positionBoyle = 0

    removableStrings = ['2020', '2021', '01',
                        '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', 'horse', 'racing', 'winner']

    for i in individualRaceLinks:
        while True:
            pullBoyleOdds = False
            pullBet365Odds = False
            Boyle = ""
            Bet365 = ""

            try:
                proxy_index = random.randint(0, len(proxies) - 1)
                proxy = {"https": proxies[proxy_index]}
                print(str(proxy))
                print("making request")
                individualRaceLinks = s.get('https://www.oddschecker.com' +
                                            i, timeout=(1, 10), headers={'User-Agent': headers}, proxies=proxy)
                print("request success")
                break
            except requests.exceptions.Timeout:
                print("##################### ERROR : Timeout #####################")
                pass
            except requests.exceptions.ConnectionError:
                print(
                    "##################### ERROR : ConnectionError #####################")
                pass

        soup = BeautifulSoup(
            individualRaceLinks.text, "html.parser")

        tableBoyle = soup.findAll('td', attrs={"data-bk": "BY"})
        tableBoyle.append(soup.findAll('td', attrs={"data-o": "BY"}))

        tableBet365 = soup.findAll('td', attrs={"data-bk": "B3"})
        tableBet365.append(soup.findAll('td', attrs={"data-o": "B3"}))

        print(i)

        if len(tableBoyle) > 3:
            try:
                Boyle = tableBoyle[3].text
            except AttributeError:
                Boyle = tableBoyle[2].text
                print(
                    "##################### ERROR : AttributeError #####################")

        if len(tableBet365) > 3:
            try:
                Bet365 = tableBet365[3].text
            except AttributeError:
                Bet365 = tableBoyle[2].text
                print(
                    "##################### ERROR : AttributeError #####################")

        title = i.replace("/", " ")
        title = title.replace("-", " ")

        titleWords = title.split()
        resultwords = [
            word for word in titleWords if word.lower() not in removableStrings]
        title = ' '.join(resultwords)

        if Boyle == 'SP' and Bet365 == 'SP' or Boyle == '' and Bet365 == '':
            pass
        else:

            Boyle = Boyle.replace("/", "")
            Bet365 = Bet365.replace("/", "")

            if Boyle.isdigit():
                pullBoyleOdds = True
            if Bet365.isdigit():
                pullBet365Odds = True

            if Boyle.isdigit() and Bet365.isdigit():
                removeFoundLinksList.append(i)

            if pullBet365Odds:

                SP_Bet365[positionBet365] = Bet365

                messageBet365 = title
                messagePassBet365 = messageBet365 + " PASS"

                if messagePassBet365 not in messageListBet365:
                    messageListBet365.append(messageBet365)

                positionBet365 = positionBet365 + 1

                pullBet365Odds = False

            if pullBoyleOdds:

                SP_Boyle[positionBoyle] = Boyle

                messageBoyle = title
                messagePassBoyle = messageBoyle + " PASS"

                if messagePassBoyle not in messageListBoyle:
                    messageListBoyle.append(messageBoyle)

                positionBoyle = positionBoyle + 1

                pullBoyleOdds = False


# remove all random shit horse races around the world
# by going through all races and keeping the ones in tracks array
def FilterUKTracks():
    for i in tracks:
        for j in allTracks:
            if i in j:
                racesHolder.append(j)

    # remove these plz so far ascot-aus newcastle-aus warwick-aus hamilton-aus

    for i in racesHolder:
        if search("aus" or "ascot-park", i):
            racesHolder.remove(i)

# remove races that have a date in their url
# indicating they're NOT today's races


def GetTomorrowsRaces(racesHolder, races):
    tomorrowDT = datetime.now() + timedelta(days=1)
    tomorrow = tomorrowDT.strftime("20" + "%y-%m-%d")

    for i in racesHolder:
        if tomorrow in i:
            races.append(i)


def WriteNotification(messageListBet365, messageListBoyle):
    global individualRaceLinks
    global SP_Bet365
    global atEveryone
    eventsAndTimes = []
    i = 0
    j = 0

    # set the size of the new array to the size of the races amount
    for x in range(len(individualRaceLinks)):
        eventsAndTimes.append(x)

    # create the discord message in a text file
    with open("/Bet365.txt", "a") as f:
        for x in messageListBet365:
            if "PASS" not in messageListBet365[i]:
                if "bangor" in messageListBet365[i]:
                    messageListBet365[i] = "bangor-on-dee " + \
                        messageListBet365[i][-5:]
                f.write(messageListBet365[i] + '\n')
                messageListBet365[i] = messageListBet365[i] + " PASS"
            i += 1
        f.close()

    with open("/Boyle.txt", "a") as f:
        for x in messageListBoyle:
            if "PASS" not in messageListBoyle[j]:
                if "bangor" in messageListBoyle[j]:
                    messageListBoyle[j] = "bangor-on-dee " + \
                        messageListBoyle[j][-5:]
                f.write(messageListBoyle[j] + '\n')
                messageListBoyle[j] = messageListBoyle[j] + " PASS"
            j += 1
        f.close()

    j = 0
    i = 0


print("running scraper...")

while(loop):
    GetTime()
    if scanSite == True:
        if dailyScan == True:
            OddsLandingPage(allTrackURLs, headers, allTracks, raceLinksWithTrash,
                            racesHolder, races, individualRaceLinks, SP_Bet365, SP_Boyle, messageListBet365, messageListBoyle)
            print("OddsLandingPage complete")
            RemoveURLTagging(allTrackURLs)
            print("RemoveURLTagging complete")
            FilterUKTracks()
            print("FilterUKTracks complete")
            GetTomorrowsRaces(racesHolder, races)
            print("GetTomorrowsRaces complete")
            GrabAllRaceURLs(headers, raceLinksWithTrash,
                            individualRaceLinks, SP_Bet365)
            print("GrabAllRaceURLs complete")
            dailyScan = False

        CheckOdds(individualRaceLinks, SP_Bet365,
                  messageListBet365, messageListBoyle, removeFoundLinksList)

        individualRaceLinksHOLDER = []
        x = 0

        for i in individualRaceLinks:
            if i not in removeFoundLinksList:
                individualRaceLinksHOLDER.append(i)

        individualRaceLinks = []

        for i in individualRaceLinksHOLDER:
            individualRaceLinks.append(i)

        time = datetime.now()
        timeNOW = time.strftime("%H:%M:%S")
        print("Odds Checked" + " " + timeNOW)

        WriteNotification(messageListBet365, messageListBoyle)

    t.sleep(60)
