import os
import discord
import asyncio
import aiohttp
import csv
import re
from discord.ext import commands, tasks
from discord.ext.commands import Bot
from collections import defaultdict
from datetime import datetime, date, time, timedelta


client = discord.Client()

bot = commands.Bot(command_prefix='#')

tomorrowsRaces = []
tomorrowsRaces_SORTED = []
tomorrowsAlerts = []

contentBoyleList = []
contentBet365List = []
contentBet365 = []
contentBoyle = []
contentBet365Holder = []
contentBoyleHolder = []
messages365 = []
messagesBoyle = []

positionBet365 = 0
positionBoyle = 0

EP_SYSTEMS = ("JR-TN2", "JR-DTR", "JR-MR3.2", "JR - LT6R", "JR - ACCAS", "jockey+pace", "JR-JLT2")

@client.event
async def new_prices():
    await client.wait_until_ready()
    global positionBet365
    global positionBoyle
    global messages365
    global messagesBoyle
    channel365 = client.get_channel(755835271198801981)
    channelBoyle = client.get_channel(758381858349383681)

    with open("/Bet365.txt", "r") as f:
        contentBet365List = f.readlines()

        contentBet365Holder = [x.strip() for x in contentBet365List]


        if len(contentBet365Holder) > 0:
            for x in contentBet365Holder:
                contentBet365.append(x)

        f.close()

        for i in contentBet365:
            for j in tomorrowsAlerts:
                if i in j and j.title() not in messages365:
                    messages365.append(j.title())
                    course = j[:-6]
                    races_on_same_course = {
                        race.title() for race in tomorrowsAlerts if course in race}
                    if races_on_same_course.issubset(set(messages365)):
                        messages365.append(
                            f"<@&771047883633328189> {course.title()} all priced.")
                    break

        if len(messages365) > positionBet365:
            await channel365.send(messages365[positionBet365])
            positionBet365 = positionBet365 + 1

        if (len(contentBet365Holder) > 0):            
            print("_____________ Bet365 ____________")
            for _ in contentBet365Holder:
                print(_)
            contentBet365Holder = []

    open("/Bet365.txt", "w").close()

    with open("/Boyle.txt", "r") as f:
        contentBoyleList = f.readlines()

        contentBoyleHolder = [x.strip() for x in contentBoyleList]

        if len(contentBoyleHolder) > 0:
            for x in contentBoyleHolder:
                contentBoyle.append(x)

        f.close()

        for i in contentBoyle:
            for j in tomorrowsAlerts:
                if i in j and j.title() not in messagesBoyle:
                    messagesBoyle.append(j.title())
                    course = j[:-6]
                    races_on_same_course = {
                        race.title() for race in tomorrowsAlerts if course in race}
                    if races_on_same_course.issubset(set(messagesBoyle)):
                        messagesBoyle.append(
                            f"<@&771053592043585566> {course.title()} all priced.")
                    break

        if len(messagesBoyle) > positionBoyle:
            await channelBoyle.send(messagesBoyle[positionBoyle])
            positionBoyle = positionBoyle + 1
            
        if (len(contentBoyleHolder) > 0):
            print("_____________ Boyle _____________")
            for _ in contentBoyleHolder:
                print(_)
            contentBoyleHolder = []

    open("/Boyle.txt", "w").close()


@client.event
async def on_message(message):

    if message.channel.name == 'csv-files':
        try:
            url = message.attachments[0].url

            async with aiohttp.ClientSession() as session:
                async with session.get(url) as response:
                    if response.status == 200:
                        f = open(
                            '/races.csv', mode='wb')
                        f.write(await response.read())
                        f.close()
                        await asyncio.sleep(3)
                        await Tomorrows_Horses()
        except IndexError:
            pass


@ client.event
async def Tomorrows_Horses():
    await client.wait_until_ready()
    columns = defaultdict(list)
    tracks = []
    i = 0

    global tomorrowsRaces
    global tomorrowsAlerts

    print("")
    print("_____________New CSV added_____________")

    await RESET()

    with open('/races.csv') as f:
        reader = csv.DictReader(f)  # read rows into a dictionary format
        # read a row as {column1: value1, column2: value2,...}
        for row in reader:
            for (k, v) in row.items():  # go over each column name and value
                # append the value into the appropriate list
                columns[k].append(v)

    for track, system in zip(columns['FUTURE_VENUE'], columns['FUTURE_SYSTEM']):
        if system not in EP_SYSTEMS:
            continue
        if track not in tracks:
            tracks.append(track)

    tracks = [item.lower() for item in tracks]

    for system in columns['FUTURE_SYSTEM']:
        if system not in EP_SYSTEMS:
            i += 1
            continue
        tomorrowsRaces.append(columns['FUTURE_VENUE']
                              [i] + " " + columns['FUTURE_TIME'][i] + " " + columns['FUTURE_HORSE'][i])

        tomorrowsAlerts.append(columns['FUTURE_VENUE']
                               [i] + " " + columns['FUTURE_TIME'][i])
        i = i + 1
    i = 0

    tomorrowsRaces = [item.lower() for item in tomorrowsRaces]
    tomorrowsAlerts = [item.lower() for item in tomorrowsAlerts]

    for x in tracks:
        for j in tomorrowsRaces:
            if x in j:
                tomorrowsRaces_SORTED.append(j)

    tomorrowsAlerts = list(dict.fromkeys(tomorrowsAlerts))

    print("tomorrowsRaces")

    for _ in tomorrowsRaces_SORTED:
        print(_)

    os.remove("/races.csv")


async def new_prices_loop(timeout, new_prices):
    while True:
        await asyncio.sleep(timeout)
        await client.loop.create_task(new_prices())

client.loop.create_task(new_prices_loop(2, new_prices))


async def ResetEverything():
    # get current computer time and return string as eg. "1732" for 5:32pm
    now = datetime.now()
    currentTime = now.strftime("%H:%M:%S")
    currentTime = re.sub(':', '', currentTime)
    currentTime = currentTime[:-2]

    if (int(currentTime) >= 2354 and int(currentTime) <= 2355):
        print("Purging...")
        purgeChannelCSV = client.get_channel(760717890503573535)
        purgeChannel365 = client.get_channel(755835271198801981)
        purgeChannelBoyle = client.get_channel(758381858349383681)
        purgeChannelNewQuals = client.get_channel(758285566264606802)

        await purgeChannelCSV.purge(limit=2)
        await purgeChannel365.purge(limit=100)
        await purgeChannelBoyle.purge(limit=100)
        await purgeChannelNewQuals.purge(limit=100)

        await RESET()


async def ResetEverything_loop(timeout):
    while True:
        await asyncio.sleep(timeout)
        await client.loop.create_task(ResetEverything())

client.loop.create_task(ResetEverything_loop(31))


async def RESET():
    global tomorrowsRaces
    global tomorrowsAlerts
    global tomorrowsRaces_SORTED
    global contentBoyleList
    global contentBet365List
    global contentBet365
    global contentBoyle
    global positionBet365
    global positionBoyle
    global contentBoyleHolder
    global contentBet365Holder

    tomorrowsRaces_SORTED = []
    tomorrowsRaces = []
    tomorrowsAlerts = []
    contentBoyleList = []
    contentBet365List = []
    contentBet365Holder = []
    contentBoyleHolder = []
    contentBet365List = []


print("running discord bot...")
client.run(
    '')
